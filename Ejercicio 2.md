Ejercicio 2. Problemas y Posibles mejoras detectadas:

- La interfaz Figure contiene métodos que Tree no implementa, debemos crear una interfaz distinta para dichos métodos, y eliminarlos de Tree.
- Definir la propiedad "$position" para Person y para Tree.
- El método getPosition de la interface no está implementado en ninguna de las dos clases.
- El método changeColor debería por coherencia llamarse setColor y utilizarse en el constructor de Person en vez de asignar un valor directamente.
- El método jump en Person está inacabado y el comentario debería ser un TODO indicándolo al menos.
- En Main, se instancian Person y Tree sin pasar parámetros y las propiedades no se inicializan con un valor por defecto. Por tanto la función draw() lanzará excepción al intentar acceder a position. A modo de ejemplo les he añadido valores, pero supongo deberían leerse de algún fichero o base de datos.
- En Main, debe comprobarse al llamar a jump y changeColor(setColor) que $figure es de la interfaz correspondiente. Creamos una tercera interface para el metodo setColor.
- Se entiende que habría una clase Position con propiedades: x, y, rotación; y métodos "move", "rotate". Podría ser conveniente englobar "x" e "y" de Position en una clase Point.
- Cambio de variables protected por privates ya que no existe herencia y no se recomienda el uso de protected.


```php
interface Figure
{
	public function getName();

	public function getPosition();

	public function rotate($degrees);

}
```

```php
interface Kinetic
{
	public function jump();

	public function walk();

}
```

```php
interface Colored
{
	public function setColor();
}
```

```php
class Person implements Figure, Kinetic, Colored
{
	private $name;
	private $position;
	private $color;
	private $acceleration = 1;

	public function __construct($name, $position, $color)
	{
		$this->setName($name);
		$this->setPosition($position);
		$this->setColor($color);
	}

	public function rotate($degrees)
	{
		$position->rotate($degrees);
	}

	public function jump()
	{
		$x = $position->getX();
		$x += $someWeirdNumber  /*TODO some math calculations */;
		$position->setX($x);
	}

	public function walk()
	{
		$position->move($this->acceleration);
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getPosition($position)
	{
		return $this->position;
	}

	public function setPosition($position)
	{
		$this->position = $position;
	}

	public function setColor($color)
	{
		$this->color = $color;
	}

}
```

```php
class Tree implements Figure
{
	private $name;
	private $position;

	public function __construct($name, $position)
	{
		$this->setName($name);
		$this->setPosition($position);
	}

	public function rotate()
	{
		$position->rotate($degrees);
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getPosition($position)
	{
		return $this->position;
	}

	public function setPosition($position)
	{
		$this->position = $position;
	}
}
```

```php
class Main
{
	private $figures = array();

	public static function game()
	{
		$figures[] = new Person("Player",new Position(0,0,0), new WhiteColor());
		$figures[] = new Tree('Tree',new Position(0,1,0));
		GameScreen::init('800', '600');
	}

	public function draw()
	{
		foreach ($this->figures as $figure) {
			$isKinematic = $figure instanceof Kinetic;
			$isColored = $figure instanceof Colored;

			if ($isKinematic && Game::detectKeyJump()) {
				$figure->jump();
			}

			printFigure($figure)

			if ($isColored && checkOutOfScreen($figure)) {
				$figure->setColor(new RedColor());
			}
		}
	}

	private function printFigure($figure){
		$x = $figure->getPosition()->getX();
		$y = $figure->getPosition()->getY();
		$name = $figure->getName();
		GameScreen::put($x, $y, $name);
	}

	private function checkOutOfScreen($figure){
		return $figure->getPosition()->getX() > GameScreen::getWith();
	}
}
```
