Ejercicio 1

Problemas detectados:

- No se devuelve respuesta a menos que el login funcione.
- Vulnerabilidad de inyección de SQL al concatenar los parametros de $_GET en la query. Se escaparán dichos valores.
- Según el código, la password estaría guardada sin ningún hash en la base de datos. En el código se tratará como si el error hubiera sido corregido.

Posibles mejoras detectadas:

- Importar las clases arriba para evitar llamar a la ruta completa, como ocurre con "Monolog\Logger".
- Extraer los logs a una clase adaptador porque el nivel de abstracción se complica demasiado.
- Extraer la ruta del log ('/var/log/user_access.log') y los valores de conexión de base de datos como variables de entorno.
- Extraer la conexión a base de datos a otra clase (MysqliConnector) y las consultas a una clase repositorio (UserRepository) para abstraer al controlador.
- Recoger los valores de $_GET en otras variables para mejorar legibilidad.
- Cambiar el naming de las variables $db, $r1 y $r2 para expresar mejor su función.
- Exceso de anidación, intentar reducirla extrayendo funciones y mediante clausulas de guardia.
- Instanciar el logger justo antes de ser utilizado en vez de al principio del controlador, ya que sólo se utiliza una vez.


```php

namespace App\Controller;

use App\Intrastructure\Logger\UserLogger;
use App\Intrastructure\Repository\UserRepository;


class LoginController extends Controller
{
	public function login()
	{
		$email = $_GET['email'];
		$password = $_GET['password'];
		if(!validateCredentials($email,$password)){
			return $this->view->render();
		}
		session_start();
		$this->logAccess($email);
		http_redirect('home');
	}

	private function validateCredentials($email,$password) : boolean
	{
		if (empty($email) || empty($password)){
			return false;
		}
		$repository = new UserRepository();
		$user = $repository->findByEmail($email);
		if(!$user){
			$this->view->message =  "el email no existe";
			return false;
		}
		if(!password_verify($password,$user->password)){
			$this->view->message =  "password incorrecto";
			return false;
		}
		return true;
	}

	private function logAccess($email){
		$logger = new UserLogger();
		$logger->logInfo('Usuario autenticado: %email%', ['email' => $email]);
	}
}
```

```php

namespace App\Intrastructure\Logger;

use Monolog\Logger;

class UserLogger
{
	private $userLogPath;
	private $logger;

	public function __construct()
	{
		$this->$userLogPath = $_ENV['USER_ACCESS_LOG'];
		$this->logger = new Logger('userLog', [new StreamHandler($this->$userLogPath)]);
	}

	public function logInfo($message, array $context = []): void
	{
		$this->logger->log(Logger::INFO, (string) $message, $context);
	}
}
```

```php

namespace App\Infrastructure\Persistence;

class MysqliConnector{
	private $server;
	private $user;
	private $pass;
	private $database;
	private $connection;

	public function __construct(){
		$this->server = $_ENV['MYSQL_SERVER'];
		$this->user = $_ENV['MYSQL_USER'];
		$this->pass = $_ENV['MYSQL_PASSWORD'];
		$this->database = $_SERVER['MYSQL_DATABASE'];
		$this->connection = mysqli_connect($this->server, $this->user, $this->pass, $this->database);
	}

	public function getConnection(){
		return $this->connection;
	}

}
```

```php

namespace App\Infrastructure\Repository;

use App\Infrastructure\Persistence\MysqliConnector;

class UserRepository{
	private $connection;

	public function __construct(MysqliConnector $connector){
		$this->connection = $connector->getConnection();
	}

	public function findByEmail($email){
		$stmt = $this->connection->prepare("SELECT * FROM users WHERE email = ?");
		$stmt->bind_param($email)
		$stmt->execute();
		$user = $stmt->get_result()->fetch_object();
		$stmt->close();
		return $user;
	}
}
```

```
USER_ACCESS_LOG=/var/log/user_access.log
MYSQL_SERVER=84.10.20.87
MYSQL_USER=root
MYSQL_PASSWORD=admin1234
MYSQL_DATABASE=db
```
