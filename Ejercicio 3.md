Problemas detectados:

- Esta clase hace demasiadas cosas ya que codifica para distintos formatos e iría creciendo con cada nuevo formato que necesitaramos.
El código existente debería ser modificado cada vez, el switch crecería y la función encode se llenaría de comprobaciones if.
- La clase DataEncoder no debería conocer los detalles sobre cada tipo de formato, deberían estar separados en clases y comunicarse por medio de una interfaz.

```php

class DataEncoder
{
	public __construct(Encoder $encoder){
		$this->encoder = $encoder;
	}

    public function encode($data, string $format): string
    {
        return $this->encoder->encode($data);
    }
}
```

```php

interface Encoder{
	public function encode($data);
}
```

```php
class JsonEncoder implements Encoder{

	public function encode($data){
		$data = $this->prepare($data);
		# Resto del código
	}

	private function prepare($data);
}
```

```php
class XmlEncoder implements Encoder{

	public function encode($data){
		$data = $this->prepare($data);
		# Resto del código
	}

	private function prepare($data);
}
```
